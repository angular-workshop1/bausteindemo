import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.css']
})
export class DemoComponent implements OnInit {

  interpolation = 'Hallo Welt';
  url = '../../assets/ape.gif';
  inputfield = 'Hier steht ein Text';
  array=[1,2,3];
  komplexesObjekt = {
    wert: 132
  };


  constructor() { }

  ngOnInit(): void {
  }

  public handleClick(){
    alert('Hi');
  }
}
